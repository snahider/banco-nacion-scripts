# ***************************************************************
# gitlab-ctl es el principal comando para interactuar con Gitlab
# ***************************************************************

# ***************************************************************
# Starting and Stopping
# ***************************************************************

# Start all GitLab components
sudo gitlab-ctl start

# Stop all GitLab components
sudo gitlab-ctl stop

# Restart all GitLab components
sudo gitlab-ctl restart

# Restart individual components
sudo gitlab-ctl restart nginx

## Send the SIGKILL signal to service
sudo gitlab-ctl kill <service>

# ***************************************************************
# Services Status
# ***************************************************************

# Get Service Status
sudo gitlab-ctl status

# ***************************************************************
# GitLab Reconfigure
# [Se debe reconfigurar GitLab cuando su configuración (/etc/gitlab/gitlab.rb) cambie]
# ***************************************************************

## Reconfigure
sudo gitlab-ctl reconfigure

# ***************************************************************
# Logs
# ***************************************************************

# Tail all logs; press Ctrl-C to exit
sudo gitlab-ctl tail

# Drill down to a sub-directory of /var/log/gitlab
sudo gitlab-ctl tail gitlab-rails

# Drill down to an individual file
sudo gitlab-ctl tail nginx/gitlab_error.log